package com.plusitsolution.project1.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.plusitsolution.common.toolkit.PlusCSVBuilder;
import com.plusitsolution.common.toolkit.PlusCSVUtils;
import com.plusitsolution.common.toolkit.PlusFileUtils;
import com.plusitsolution.common.toolkit.PlusJsonUtils;
import com.plusitsolution.project1.domain.ManageCsvDomain;
import com.plusitsolution.project1.domain.PersonDomain;
import com.plusitsolution.project1.domain.ShelfShareDomain;

@Service
public class ManageCsvService {

//	HashMap<String, HashMap<String, Object>> SHOP_MAP = new HashMap<String, HashMap<String, Object>>();
//	// เอา map ล่างไปใส่ map บน
//	Map<String, Object> SHELFSHARE_MAP = new HashMap<String, Object>();
//
//	public void downloadCSVA(MultipartFile uploadfile) throws Exception {
//		// เอาไฟล์ที่อัพไปไว้ใน
////		
////		String json = "\"skuCounts\":{\"SNACK\":{\"MILO_CEREAL\":0,\"KOKO_KRUNCH\":0,\"COPP_CEREAL\":0}}";
////		Map<String, Object> student2 = PlusJsonUtils.convertJsonStringToMap(json);
////		System.out.println(student2.get("skuCounts"));
//
//		File file = new File(
//				"C:\\Users\\ACER\\eclipse-workspace\\project1\\src\\main\\java\\com\\plusitsolution\\project1\\fileUpload\\Output.csv");
//		uploadfile.transferTo(file);
//		System.out.println(file.getAbsolutePath() + " " + file.exists());
//		// อ่านไฟล์
//		PlusCSVBuilder csv = PlusCSVUtils.csv(
//				"C:\\Users\\ACER\\eclipse-workspace\\project1\\src\\main\\java\\com\\plusitsolution\\project1\\fileUpload\\Output.csv");
//		List<String[]> lines = csv.read();
//
////		for (String[] line : lines) {
////			System.out.println(line[15]);
////			ShelfShareDomain domain = PlusJsonUtils.convertToJsonObject(ShelfShareDomain.class, line[15]);
////			}
//
//		for (int i = 0; i < lines.size(); i++) {
////			System.out.println(line[15]);
////			Map<String, Map<String, ShelfShareDomain>>  Shelf_Share = PlusJsonUtils.convertJsonStringToMap(lines.get(i)[15]);
////			Map<String, Object> shel = PlusJsonUtils.convertToJsonObject(ShelfShareDomain.class, line[15]).get;
////			ShelfShareDomain domain =  (ShelfShareDomain) PlusJsonUtils.convertJsonStringToMap(lines.get(i)[15]);
////			ManageCsvDomain domain = PlusJsonUtils.convertToJsonObject(ManageCsvDomain.class, lines.get(i)[15]);
////			ManageCsvDomain domain = PlusJsonUtils.convertToJsonObject(ManageCsvDomain.class, lines.get(i)[15]);
////			ManageCsvDomain domain =  (ManageCsvDomain) PlusJsonUtils.convertJsonStringToMap(lines.get(i)[15]);
//			SHELFSHARE_MAP = PlusJsonUtils.convertJsonStringToMap(lines.get(i)[15]);
//
////			System.out.println(SHELFSHARE_MAP.get("skuCounts"));
//		}
//	}

	public HttpEntity<byte[]> downloadCSV(MultipartFile uploadfile) throws Exception {

		File file = new File(
				"C:\\Users\\ACER\\eclipse-workspace\\project1\\src\\main\\java\\com\\plusitsolution\\project1\\fileUpload\\output.csv");
		uploadfile.transferTo(file);
		System.out.println(file.getAbsolutePath() + " " + file.exists());
		PlusCSVBuilder csv = PlusCSVUtils.csv(
				"C:\\Users\\ACER\\eclipse-workspace\\project1\\src\\main\\java\\com\\plusitsolution\\project1\\fileUpload\\output.csv");
		List<String[]> lines = csv.read();

		String[] csvHeaders = { "count", "id", "age", "name and address" };
		PlusCSVBuilder builder = PlusCSVUtils.csv(new ByteArrayInputStream(new byte[128])).headers(csvHeaders);
		Integer sumAge = 0;
		int i;
		int age;
		for (i = 0; i < lines.size(); i++) {
			String studentID = lines.get(i)[0];
			String nameAndAddress = lines.get(i)[1] + "from" + lines.get(i)[3];
			age = Integer.parseInt(lines.get(i)[2].trim());
			sumAge += age;
			
			int count = i + 1;
			builder.line(count, studentID, age, nameAndAddress);

		}
		builder.line(" ", " ", sumAge, " ");
		byte[] content = builder.writeBytes();
		HttpHeaders header = new HttpHeaders();
		header.set("charset", "UTF-8");
		header.set(HttpHeaders.CONTENT_ENCODING, "UTF-8");
		header.set(HttpHeaders.CONTENT_TYPE, "text/csv; charset=UTF-8;");
		header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; charset=UTF-8; filename=output.csv");
//		PlusFileUtils.delete(file);
		return new HttpEntity<byte[]>(content, header);
	}
}
