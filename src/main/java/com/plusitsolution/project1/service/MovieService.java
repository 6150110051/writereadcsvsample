package com.plusitsolution.project1.service;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.plusitsolution.common.toolkit.PlusCSVBuilder;
import com.plusitsolution.common.toolkit.PlusCSVUtils;
import com.plusitsolution.common.toolkit.PlusJsonUtils;
import com.plusitsolution.project1.domain.MovieDomain;
import com.plusitsolution.project1.domain.PersonDomain;
@Service
public class MovieService {
	 public Integer counter = 0;

	    public MovieDomain registerMovieDomain(String name, Integer cost, MultipartFile file){
	    	
	        return new MovieDomain(name, cost);
	    }
	    
	    public void test(MultipartFile file) throws Exception {
	    	File targetFile = new File("/Users/leng/Desktop/vault-docker/temp.csv");
	    	file.transferTo(targetFile);
	    	System.out.println(targetFile.getAbsolutePath());
	    	System.out.println(PlusCSVUtils.csv(targetFile.getAbsolutePath()).read().get(0)[0]);
	    	System.out.println(PlusCSVUtils.csv(targetFile.getAbsolutePath()).read().get(0)[1]);
	    	targetFile.delete();
	    }
	    
	    public void test2() {
	    	String json = "{\n"
	    			+ "	\"name\": \"leng\",\n"
	    			+ "	\"age\": 1  ,\n"
	    			+ "	\"subject\": [\"math\",\"eng\"]\n"
	    			+ "}";
	    	PersonDomain domain = PlusJsonUtils.convertToJsonObject(PersonDomain.class, json);
	    }
	    

//	    public static void main(String[] args) {
//	    	String json = "{\n"
//	    			+ "	\"name\": \"leng\",\n"
//	    			+ "	\"age\": 1  ,\n"
//	    			+ "	\"subject\": [\"math\",\"eng\"]\n"
//	    			+ "}";
//	    	PersonDomain domain = PlusJsonUtils.convertToJsonObject(PersonDomain.class, json);
//	    	System.out.println(PlusJsonUtils.convertToJsonString(domain));
//		}
	    
	    public HttpEntity<byte[]> downloadCSV(){
	    	String[] csvHeaders = {"id","name","age"};
	        PlusCSVBuilder builder = PlusCSVUtils.csv(new ByteArrayInputStream(new byte[128])).headers(csvHeaders);
	        builder.line("1","leng","22");
	        builder.line("2","boss","50");
	        
	        byte[] content = builder.writeBytes();
	        HttpHeaders header = new HttpHeaders();
	        header.set("charset", "UTF-8");
	        header.set(HttpHeaders.CONTENT_ENCODING, "UTF-8");
	        header.set(HttpHeaders.CONTENT_TYPE, "text/csv; charset=UTF-8;");
	        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; charset=UTF-8; filename=output.csv");
	        return new HttpEntity<byte[]>(content,header);
	    }
}
