package com.plusitsolution.project1.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

public class readCSV {

	public static void main(String[] args) {
		
		String file = "com.plusitsolution.project1.service\\NESTLE_NESTLE20211005_AUDIT_DETAIL_20211028210335.csv";
		BufferedReader reader = null;
		String line = "";
		
		try {
			reader = new BufferedReader(new FileReader(file));
			while((line = reader.readLine())!= null) {
				String[] row = line.split(",");
				
				for(String index : row) {
					
				}
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	
	
	public static final int BOM_CHARACTER = '\ufeff';

	public static File writeCSV(String[] headers, List<Object[]> lines, Path path, boolean appendBOM) {
		CSVPrinter csvPrinter = null;
		FileWriter fileWriter = null;
		try {
			File csvFile = path.toFile();
			fileWriter = new FileWriter(path.toFile(), StandardCharsets.UTF_8);
			if (appendBOM) {
				fileWriter.write(BOM_CHARACTER);
			}
			CSVFormat csvFormat = headers == null || headers.length == 0 ? CSVFormat.DEFAULT : CSVFormat.DEFAULT.withHeader(headers);
			csvPrinter = new CSVPrinter(fileWriter, csvFormat);
			csvPrinter.printRecords(lines);
			fileWriter.flush();
			csvPrinter.flush();
			return csvFile;
		} catch (IOException e) {
			throw new RuntimeException("Cannot writeCSV", e);
		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.close();
				} catch (IOException e) {
					throw new RuntimeException("Cannot close FileWriter", e);
				}
			}
			if (csvPrinter != null) {
				try {
					csvPrinter.close();
				} catch (IOException e) {
					throw new RuntimeException("Cannot close CSVWriter", e);
				}
			}
		}
	}

	public static byte[] writeBytes(String[] headers, List<Object[]> lines, boolean appendBOM) {
		CSVPrinter csvPrinter = null;
		BufferedWriter bufferedWriter = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		try {
			byteArrayOutputStream = new ByteArrayOutputStream();
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(byteArrayOutputStream, StandardCharsets.UTF_8));
			if (appendBOM) {
				bufferedWriter.write(BOM_CHARACTER);
			}
			CSVFormat csvFormat = headers == null || headers.length == 0 ? CSVFormat.DEFAULT : CSVFormat.DEFAULT.withHeader(headers);
			csvPrinter = new CSVPrinter(bufferedWriter, csvFormat);
			csvPrinter.printRecords(lines);
			bufferedWriter.flush();
			csvPrinter.flush();
			return byteArrayOutputStream.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException("Cannot writeCSV", e);
		} finally {
			if (byteArrayOutputStream != null) {
				try {
					byteArrayOutputStream.close();
				} catch (IOException e) {
					throw new RuntimeException("Cannot close ByteArrayOutputStream", e);
				}
			}
			if (bufferedWriter != null) {
				try {
					bufferedWriter.close();
				} catch (IOException e) {
					throw new RuntimeException("Cannot close BufferedWriter", e);
				}
			}
			if (csvPrinter != null) {
				try {
					csvPrinter.close();
				} catch (IOException e) {
					throw new RuntimeException("Cannot close CSVWriter", e);
				}
			}
		}
	}

	private static List<String[]> readByHeader(String[] headers, Iterable<CSVRecord> csvRecords) {
		List<String[]> lines = new ArrayList<>();
		List<String> line;
		for (CSVRecord csvRecord : csvRecords) {
			line = new ArrayList<>();
			for (String header : headers) {
				line.add(csvRecord.get(header));
			}
			lines.add(line.toArray(new String[] {}));
		}
		return lines;
	}

	private static List<String[]> readByIndex(Iterable<CSVRecord> csvRecords) {
		List<String[]> lines = new ArrayList<>();
		List<String> line;
		for (CSVRecord csvRecord : csvRecords) {
			line = new ArrayList<>();
			Iterator<String> iterator = csvRecord.iterator();
			while (iterator.hasNext()) {
				line.add(iterator.next());
			}
			lines.add(line.toArray(new String[] {}));
		}
		return lines;
	}

	public static List<String[]> readCSV(String[] headers, Reader in, boolean skipFirstLine) {
		try {
			boolean hasHeader = headers != null && headers.length > 0;
			CSVFormat csvFormat = (skipFirstLine ? CSVFormat.DEFAULT.withFirstRecordAsHeader() : hasHeader ? CSVFormat.DEFAULT.withHeader(headers) : CSVFormat.DEFAULT).withIgnoreEmptyLines();
			Iterable<CSVRecord> records = csvFormat.parse(in);
			return hasHeader ? readByHeader(headers, records) : readByIndex(records);
		} catch (Exception e) {
			throw new RuntimeException("Cannot readCSV", e);
		}
	}



}
