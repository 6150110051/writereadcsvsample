package com.plusitsolution.project1.domain;

public class ShelfShareDomain {
	private String name;
	private Integer quantity;
	
	
	
	public ShelfShareDomain(String name, Integer quantity) {
		super();
		this.name = name;
		this.quantity = quantity;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
	
}
