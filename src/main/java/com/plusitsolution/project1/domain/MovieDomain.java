package com.plusitsolution.project1.domain;

public class MovieDomain {
	private String name;
	private Integer cost;
	public MovieDomain(String name, Integer cost) {
		super();
		this.name = name;
		this.cost = cost;
	}
	
	public MovieDomain() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}
	
	
}
