package com.plusitsolution.project1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.plusitsolution.project1.service.ManageCsvService;


@RestController
public class ManageCsvController {
	
	@Autowired
    private ManageCsvService ManageCsvService;
	
	
//	 @PostMapping(path = "/sampleUploadFile", consumes =  MediaType.MULTIPART_FORM_DATA_VALUE )
//	    public void sampleUploadFile(@RequestParam("uploadFile") MultipartFile uploadfile) throws Exception {
////	    	return ManageCsvService.downloadCSV(uploadfile);
//	    }
	    
	 
	    
	    
	 @PostMapping(path = "/downloadCSV", consumes =  MediaType.MULTIPART_FORM_DATA_VALUE )
	    public HttpEntity<byte[]>  downloadCSV(@RequestParam("uploadFile") MultipartFile uploadfile) throws Exception {
	    	
	    	return ManageCsvService.downloadCSV(uploadfile);
	    }
	
}
